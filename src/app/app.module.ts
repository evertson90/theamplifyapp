import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomecardComponent } from './components/home/homecard/homecard.component';
import { ApiComponent } from './components/categories/api/api.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AddPostComponent } from './components/categories/api/add-post/add-post.component';
import { PostsComponent } from './components/categories/api/posts/posts.component';
import { LikesComponent } from './components/categories/api/likes/likes.component';
import { CommentsComponent } from './components/categories/api/comments/comments.component';
import { AmplifyAuthenticatorModule } from '@aws-amplify/ui-angular';
import { StorageComponent } from './components/categories/storage/storage.component';
import { ImageUploadComponent } from './components/categories/storage/image-upload/image-upload.component';
import { ImageAlbumComponent } from './components/categories/storage/image-album/image-album.component';
import { PredictionsComponent } from './components/categories/predictions/predictions.component';
import { IdentifyTextComponent } from './components/categories/predictions/identify-text/identify-text.component';
import { TextToSpeechComponent } from './components/categories/predictions/text-to-speech/text-to-speech.component';
import { TextInterpretComponent } from './components/categories/predictions/text-interpret/text-interpret.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    HomecardComponent,
    ApiComponent,
    AddPostComponent,
    PostsComponent,
    LikesComponent,
    CommentsComponent,
    StorageComponent,
    ImageUploadComponent,
    ImageAlbumComponent,
    PredictionsComponent,
    IdentifyTextComponent,
    TextToSpeechComponent,
    TextInterpretComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    AmplifyAuthenticatorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
