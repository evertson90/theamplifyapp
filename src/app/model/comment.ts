export interface Comment {
  postId: string;
  user: string;
  date: Date;
  content: string;
}
