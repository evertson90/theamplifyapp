export interface Post {
  id: string;
  title: string;
  description: string;
  author: string;
  date: Date;
  likes: number;
  comments: number;
}
