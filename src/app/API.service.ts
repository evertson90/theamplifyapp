/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.
import { Injectable } from "@angular/core";
import API, { graphqlOperation, GraphQLResult } from "@aws-amplify/api-graphql";
import { Observable } from "zen-observable-ts";

export interface SubscriptionResponse<T> {
  value: GraphQLResult<T>;
}

export type __SubscriptionContainer = {
  onCreatePost: OnCreatePostSubscription;
  onUpdatePost: OnUpdatePostSubscription;
  onDeletePost: OnDeletePostSubscription;
  onCreateLike: OnCreateLikeSubscription;
  onUpdateLike: OnUpdateLikeSubscription;
  onDeleteLike: OnDeleteLikeSubscription;
  onCreateComment: OnCreateCommentSubscription;
  onUpdateComment: OnUpdateCommentSubscription;
  onDeleteComment: OnDeleteCommentSubscription;
};

export type ModelPostFilterInput = {
  id?: ModelIDInput | null;
  title?: ModelStringInput | null;
  description?: ModelStringInput | null;
  and?: Array<ModelPostFilterInput | null> | null;
  or?: Array<ModelPostFilterInput | null> | null;
  not?: ModelPostFilterInput | null;
};

export type ModelIDInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
  size?: ModelSizeInput | null;
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null"
}

export type ModelSizeInput = {
  ne?: number | null;
  eq?: number | null;
  le?: number | null;
  lt?: number | null;
  ge?: number | null;
  gt?: number | null;
  between?: Array<number | null> | null;
};

export type ModelStringInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
  size?: ModelSizeInput | null;
};

export type ModelPostConnection = {
  __typename: "ModelPostConnection";
  items: Array<Post | null>;
  nextToken?: string | null;
};

export type Post = {
  __typename: "Post";
  id: string;
  title: string;
  description?: string | null;
  likes?: ModelLikeConnection | null;
  comments?: ModelCommentConnection | null;
  createdAt: string;
  updatedAt: string;
  owner?: string | null;
};

export type ModelLikeConnection = {
  __typename: "ModelLikeConnection";
  items: Array<Like | null>;
  nextToken?: string | null;
};

export type Like = {
  __typename: "Like";
  id: string;
  post?: Post | null;
  createdAt: string;
  updatedAt: string;
  postLikesId?: string | null;
  owner?: string | null;
};

export type ModelCommentConnection = {
  __typename: "ModelCommentConnection";
  items: Array<Comment | null>;
  nextToken?: string | null;
};

export type Comment = {
  __typename: "Comment";
  id: string;
  post?: Post | null;
  content: string;
  createdAt: string;
  updatedAt: string;
  postCommentsId?: string | null;
  owner?: string | null;
};

export type CreatePostInput = {
  id?: string | null;
  title: string;
  description?: string | null;
};

export type ModelPostConditionInput = {
  title?: ModelStringInput | null;
  description?: ModelStringInput | null;
  and?: Array<ModelPostConditionInput | null> | null;
  or?: Array<ModelPostConditionInput | null> | null;
  not?: ModelPostConditionInput | null;
};

export type UpdatePostInput = {
  id: string;
  title?: string | null;
  description?: string | null;
};

export type DeletePostInput = {
  id: string;
};

export type CreateLikeInput = {
  id?: string | null;
  postLikesId?: string | null;
};

export type ModelLikeConditionInput = {
  and?: Array<ModelLikeConditionInput | null> | null;
  or?: Array<ModelLikeConditionInput | null> | null;
  not?: ModelLikeConditionInput | null;
  postLikesId?: ModelIDInput | null;
};

export type UpdateLikeInput = {
  id: string;
  postLikesId?: string | null;
};

export type DeleteLikeInput = {
  id: string;
};

export type CreateCommentInput = {
  id?: string | null;
  content: string;
  postCommentsId?: string | null;
};

export type ModelCommentConditionInput = {
  content?: ModelStringInput | null;
  and?: Array<ModelCommentConditionInput | null> | null;
  or?: Array<ModelCommentConditionInput | null> | null;
  not?: ModelCommentConditionInput | null;
  postCommentsId?: ModelIDInput | null;
};

export type UpdateCommentInput = {
  id: string;
  content?: string | null;
  postCommentsId?: string | null;
};

export type DeleteCommentInput = {
  id: string;
};

export type ModelLikeFilterInput = {
  id?: ModelIDInput | null;
  and?: Array<ModelLikeFilterInput | null> | null;
  or?: Array<ModelLikeFilterInput | null> | null;
  not?: ModelLikeFilterInput | null;
  postLikesId?: ModelIDInput | null;
};

export type ModelCommentFilterInput = {
  id?: ModelIDInput | null;
  content?: ModelStringInput | null;
  and?: Array<ModelCommentFilterInput | null> | null;
  or?: Array<ModelCommentFilterInput | null> | null;
  not?: ModelCommentFilterInput | null;
  postCommentsId?: ModelIDInput | null;
};

export type CustomListPostsQuery = {
  __typename: "ModelPostConnection";
  items: Array<{
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      items: Array<{
        __typename: "Like";
        id: string;
      } | null>;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      items: Array<{
        __typename: "Comment";
        id: string;
      } | null>;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null>;
  nextToken?: string | null;
};

export type CreatePostMutation = {
  __typename: "Post";
  id: string;
  title: string;
  description?: string | null;
  likes?: {
    __typename: "ModelLikeConnection";
    items: Array<{
      __typename: "Like";
      id: string;
      createdAt: string;
      updatedAt: string;
      postLikesId?: string | null;
      owner?: string | null;
    } | null>;
    nextToken?: string | null;
  } | null;
  comments?: {
    __typename: "ModelCommentConnection";
    items: Array<{
      __typename: "Comment";
      id: string;
      content: string;
      createdAt: string;
      updatedAt: string;
      postCommentsId?: string | null;
      owner?: string | null;
    } | null>;
    nextToken?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
  owner?: string | null;
};

export type UpdatePostMutation = {
  __typename: "Post";
  id: string;
  title: string;
  description?: string | null;
  likes?: {
    __typename: "ModelLikeConnection";
    items: Array<{
      __typename: "Like";
      id: string;
      createdAt: string;
      updatedAt: string;
      postLikesId?: string | null;
      owner?: string | null;
    } | null>;
    nextToken?: string | null;
  } | null;
  comments?: {
    __typename: "ModelCommentConnection";
    items: Array<{
      __typename: "Comment";
      id: string;
      content: string;
      createdAt: string;
      updatedAt: string;
      postCommentsId?: string | null;
      owner?: string | null;
    } | null>;
    nextToken?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
  owner?: string | null;
};

export type DeletePostMutation = {
  __typename: "Post";
  id: string;
  title: string;
  description?: string | null;
  likes?: {
    __typename: "ModelLikeConnection";
    items: Array<{
      __typename: "Like";
      id: string;
      createdAt: string;
      updatedAt: string;
      postLikesId?: string | null;
      owner?: string | null;
    } | null>;
    nextToken?: string | null;
  } | null;
  comments?: {
    __typename: "ModelCommentConnection";
    items: Array<{
      __typename: "Comment";
      id: string;
      content: string;
      createdAt: string;
      updatedAt: string;
      postCommentsId?: string | null;
      owner?: string | null;
    } | null>;
    nextToken?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
  owner?: string | null;
};

export type CreateLikeMutation = {
  __typename: "Like";
  id: string;
  post?: {
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      nextToken?: string | null;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
  postLikesId?: string | null;
  owner?: string | null;
};

export type UpdateLikeMutation = {
  __typename: "Like";
  id: string;
  post?: {
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      nextToken?: string | null;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
  postLikesId?: string | null;
  owner?: string | null;
};

export type DeleteLikeMutation = {
  __typename: "Like";
  id: string;
  post?: {
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      nextToken?: string | null;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
  postLikesId?: string | null;
  owner?: string | null;
};

export type CreateCommentMutation = {
  __typename: "Comment";
  id: string;
  post?: {
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      nextToken?: string | null;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null;
  content: string;
  createdAt: string;
  updatedAt: string;
  postCommentsId?: string | null;
  owner?: string | null;
};

export type UpdateCommentMutation = {
  __typename: "Comment";
  id: string;
  post?: {
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      nextToken?: string | null;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null;
  content: string;
  createdAt: string;
  updatedAt: string;
  postCommentsId?: string | null;
  owner?: string | null;
};

export type DeleteCommentMutation = {
  __typename: "Comment";
  id: string;
  post?: {
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      nextToken?: string | null;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null;
  content: string;
  createdAt: string;
  updatedAt: string;
  postCommentsId?: string | null;
  owner?: string | null;
};

export type GetPostQuery = {
  __typename: "Post";
  id: string;
  title: string;
  description?: string | null;
  likes?: {
    __typename: "ModelLikeConnection";
    items: Array<{
      __typename: "Like";
      id: string;
      createdAt: string;
      updatedAt: string;
      postLikesId?: string | null;
      owner?: string | null;
    } | null>;
    nextToken?: string | null;
  } | null;
  comments?: {
    __typename: "ModelCommentConnection";
    items: Array<{
      __typename: "Comment";
      id: string;
      content: string;
      createdAt: string;
      updatedAt: string;
      postCommentsId?: string | null;
      owner?: string | null;
    } | null>;
    nextToken?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
  owner?: string | null;
};

export type ListPostsQuery = {
  __typename: "ModelPostConnection";
  items: Array<{
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      nextToken?: string | null;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null>;
  nextToken?: string | null;
};

export type GetLikeQuery = {
  __typename: "Like";
  id: string;
  post?: {
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      nextToken?: string | null;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
  postLikesId?: string | null;
  owner?: string | null;
};

export type ListLikesQuery = {
  __typename: "ModelLikeConnection";
  items: Array<{
    __typename: "Like";
    id: string;
    post?: {
      __typename: "Post";
      id: string;
      title: string;
      description?: string | null;
      createdAt: string;
      updatedAt: string;
      owner?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    postLikesId?: string | null;
    owner?: string | null;
  } | null>;
  nextToken?: string | null;
};

export type GetCommentQuery = {
  __typename: "Comment";
  id: string;
  post?: {
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      nextToken?: string | null;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null;
  content: string;
  createdAt: string;
  updatedAt: string;
  postCommentsId?: string | null;
  owner?: string | null;
};

export type ListCommentsQuery = {
  __typename: "ModelCommentConnection";
  items: Array<{
    __typename: "Comment";
    id: string;
    post?: {
      __typename: "Post";
      id: string;
      title: string;
      description?: string | null;
      createdAt: string;
      updatedAt: string;
      owner?: string | null;
    } | null;
    content: string;
    createdAt: string;
    updatedAt: string;
    postCommentsId?: string | null;
    owner?: string | null;
  } | null>;
  nextToken?: string | null;
};

export type OnCreatePostSubscription = {
  __typename: "Post";
  id: string;
  title: string;
  description?: string | null;
  likes?: {
    __typename: "ModelLikeConnection";
    items: Array<{
      __typename: "Like";
      id: string;
      createdAt: string;
      updatedAt: string;
      postLikesId?: string | null;
      owner?: string | null;
    } | null>;
    nextToken?: string | null;
  } | null;
  comments?: {
    __typename: "ModelCommentConnection";
    items: Array<{
      __typename: "Comment";
      id: string;
      content: string;
      createdAt: string;
      updatedAt: string;
      postCommentsId?: string | null;
      owner?: string | null;
    } | null>;
    nextToken?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
  owner?: string | null;
};

export type OnUpdatePostSubscription = {
  __typename: "Post";
  id: string;
  title: string;
  description?: string | null;
  likes?: {
    __typename: "ModelLikeConnection";
    items: Array<{
      __typename: "Like";
      id: string;
      createdAt: string;
      updatedAt: string;
      postLikesId?: string | null;
      owner?: string | null;
    } | null>;
    nextToken?: string | null;
  } | null;
  comments?: {
    __typename: "ModelCommentConnection";
    items: Array<{
      __typename: "Comment";
      id: string;
      content: string;
      createdAt: string;
      updatedAt: string;
      postCommentsId?: string | null;
      owner?: string | null;
    } | null>;
    nextToken?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
  owner?: string | null;
};

export type OnDeletePostSubscription = {
  __typename: "Post";
  id: string;
  title: string;
  description?: string | null;
  likes?: {
    __typename: "ModelLikeConnection";
    items: Array<{
      __typename: "Like";
      id: string;
      createdAt: string;
      updatedAt: string;
      postLikesId?: string | null;
      owner?: string | null;
    } | null>;
    nextToken?: string | null;
  } | null;
  comments?: {
    __typename: "ModelCommentConnection";
    items: Array<{
      __typename: "Comment";
      id: string;
      content: string;
      createdAt: string;
      updatedAt: string;
      postCommentsId?: string | null;
      owner?: string | null;
    } | null>;
    nextToken?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
  owner?: string | null;
};

export type OnCreateLikeSubscription = {
  __typename: "Like";
  id: string;
  post?: {
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      nextToken?: string | null;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
  postLikesId?: string | null;
  owner?: string | null;
};

export type OnUpdateLikeSubscription = {
  __typename: "Like";
  id: string;
  post?: {
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      nextToken?: string | null;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
  postLikesId?: string | null;
  owner?: string | null;
};

export type OnDeleteLikeSubscription = {
  __typename: "Like";
  id: string;
  post?: {
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      nextToken?: string | null;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
  postLikesId?: string | null;
  owner?: string | null;
};

export type OnCreateCommentSubscription = {
  __typename: "Comment";
  id: string;
  post?: {
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      nextToken?: string | null;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null;
  content: string;
  createdAt: string;
  updatedAt: string;
  postCommentsId?: string | null;
  owner?: string | null;
};

export type OnUpdateCommentSubscription = {
  __typename: "Comment";
  id: string;
  post?: {
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      nextToken?: string | null;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null;
  content: string;
  createdAt: string;
  updatedAt: string;
  postCommentsId?: string | null;
  owner?: string | null;
};

export type OnDeleteCommentSubscription = {
  __typename: "Comment";
  id: string;
  post?: {
    __typename: "Post";
    id: string;
    title: string;
    description?: string | null;
    likes?: {
      __typename: "ModelLikeConnection";
      nextToken?: string | null;
    } | null;
    comments?: {
      __typename: "ModelCommentConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
    owner?: string | null;
  } | null;
  content: string;
  createdAt: string;
  updatedAt: string;
  postCommentsId?: string | null;
  owner?: string | null;
};

@Injectable({
  providedIn: "root"
})
export class APIService {
  async CustomListPosts(
    filter?: ModelPostFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<CustomListPostsQuery> {
    const statement = `query CustomListPosts($filter: ModelPostFilterInput, $limit: Int, $nextToken: String) {
        listPosts(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            title
            description
            likes {
              __typename
              items {
                __typename
                id
              }
            }
            comments {
              __typename
              items {
                __typename
                id
              }
            }
            createdAt
            updatedAt
            owner
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CustomListPostsQuery>response.data.listPosts;
  }
  async CreatePost(
    input: CreatePostInput,
    condition?: ModelPostConditionInput
  ): Promise<CreatePostMutation> {
    const statement = `mutation CreatePost($input: CreatePostInput!, $condition: ModelPostConditionInput) {
        createPost(input: $input, condition: $condition) {
          __typename
          id
          title
          description
          likes {
            __typename
            items {
              __typename
              id
              createdAt
              updatedAt
              postLikesId
              owner
            }
            nextToken
          }
          comments {
            __typename
            items {
              __typename
              id
              content
              createdAt
              updatedAt
              postCommentsId
              owner
            }
            nextToken
          }
          createdAt
          updatedAt
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreatePostMutation>response.data.createPost;
  }
  async UpdatePost(
    input: UpdatePostInput,
    condition?: ModelPostConditionInput
  ): Promise<UpdatePostMutation> {
    const statement = `mutation UpdatePost($input: UpdatePostInput!, $condition: ModelPostConditionInput) {
        updatePost(input: $input, condition: $condition) {
          __typename
          id
          title
          description
          likes {
            __typename
            items {
              __typename
              id
              createdAt
              updatedAt
              postLikesId
              owner
            }
            nextToken
          }
          comments {
            __typename
            items {
              __typename
              id
              content
              createdAt
              updatedAt
              postCommentsId
              owner
            }
            nextToken
          }
          createdAt
          updatedAt
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdatePostMutation>response.data.updatePost;
  }
  async DeletePost(
    input: DeletePostInput,
    condition?: ModelPostConditionInput
  ): Promise<DeletePostMutation> {
    const statement = `mutation DeletePost($input: DeletePostInput!, $condition: ModelPostConditionInput) {
        deletePost(input: $input, condition: $condition) {
          __typename
          id
          title
          description
          likes {
            __typename
            items {
              __typename
              id
              createdAt
              updatedAt
              postLikesId
              owner
            }
            nextToken
          }
          comments {
            __typename
            items {
              __typename
              id
              content
              createdAt
              updatedAt
              postCommentsId
              owner
            }
            nextToken
          }
          createdAt
          updatedAt
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeletePostMutation>response.data.deletePost;
  }
  async CreateLike(
    input: CreateLikeInput,
    condition?: ModelLikeConditionInput
  ): Promise<CreateLikeMutation> {
    const statement = `mutation CreateLike($input: CreateLikeInput!, $condition: ModelLikeConditionInput) {
        createLike(input: $input, condition: $condition) {
          __typename
          id
          post {
            __typename
            id
            title
            description
            likes {
              __typename
              nextToken
            }
            comments {
              __typename
              nextToken
            }
            createdAt
            updatedAt
            owner
          }
          createdAt
          updatedAt
          postLikesId
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateLikeMutation>response.data.createLike;
  }
  async UpdateLike(
    input: UpdateLikeInput,
    condition?: ModelLikeConditionInput
  ): Promise<UpdateLikeMutation> {
    const statement = `mutation UpdateLike($input: UpdateLikeInput!, $condition: ModelLikeConditionInput) {
        updateLike(input: $input, condition: $condition) {
          __typename
          id
          post {
            __typename
            id
            title
            description
            likes {
              __typename
              nextToken
            }
            comments {
              __typename
              nextToken
            }
            createdAt
            updatedAt
            owner
          }
          createdAt
          updatedAt
          postLikesId
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateLikeMutation>response.data.updateLike;
  }
  async DeleteLike(
    input: DeleteLikeInput,
    condition?: ModelLikeConditionInput
  ): Promise<DeleteLikeMutation> {
    const statement = `mutation DeleteLike($input: DeleteLikeInput!, $condition: ModelLikeConditionInput) {
        deleteLike(input: $input, condition: $condition) {
          __typename
          id
          post {
            __typename
            id
            title
            description
            likes {
              __typename
              nextToken
            }
            comments {
              __typename
              nextToken
            }
            createdAt
            updatedAt
            owner
          }
          createdAt
          updatedAt
          postLikesId
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteLikeMutation>response.data.deleteLike;
  }
  async CreateComment(
    input: CreateCommentInput,
    condition?: ModelCommentConditionInput
  ): Promise<CreateCommentMutation> {
    const statement = `mutation CreateComment($input: CreateCommentInput!, $condition: ModelCommentConditionInput) {
        createComment(input: $input, condition: $condition) {
          __typename
          id
          post {
            __typename
            id
            title
            description
            likes {
              __typename
              nextToken
            }
            comments {
              __typename
              nextToken
            }
            createdAt
            updatedAt
            owner
          }
          content
          createdAt
          updatedAt
          postCommentsId
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateCommentMutation>response.data.createComment;
  }
  async UpdateComment(
    input: UpdateCommentInput,
    condition?: ModelCommentConditionInput
  ): Promise<UpdateCommentMutation> {
    const statement = `mutation UpdateComment($input: UpdateCommentInput!, $condition: ModelCommentConditionInput) {
        updateComment(input: $input, condition: $condition) {
          __typename
          id
          post {
            __typename
            id
            title
            description
            likes {
              __typename
              nextToken
            }
            comments {
              __typename
              nextToken
            }
            createdAt
            updatedAt
            owner
          }
          content
          createdAt
          updatedAt
          postCommentsId
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateCommentMutation>response.data.updateComment;
  }
  async DeleteComment(
    input: DeleteCommentInput,
    condition?: ModelCommentConditionInput
  ): Promise<DeleteCommentMutation> {
    const statement = `mutation DeleteComment($input: DeleteCommentInput!, $condition: ModelCommentConditionInput) {
        deleteComment(input: $input, condition: $condition) {
          __typename
          id
          post {
            __typename
            id
            title
            description
            likes {
              __typename
              nextToken
            }
            comments {
              __typename
              nextToken
            }
            createdAt
            updatedAt
            owner
          }
          content
          createdAt
          updatedAt
          postCommentsId
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteCommentMutation>response.data.deleteComment;
  }
  async GetPost(id: string): Promise<GetPostQuery> {
    const statement = `query GetPost($id: ID!) {
        getPost(id: $id) {
          __typename
          id
          title
          description
          likes {
            __typename
            items {
              __typename
              id
              createdAt
              updatedAt
              postLikesId
              owner
            }
            nextToken
          }
          comments {
            __typename
            items {
              __typename
              id
              content
              createdAt
              updatedAt
              postCommentsId
              owner
            }
            nextToken
          }
          createdAt
          updatedAt
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetPostQuery>response.data.getPost;
  }
  async ListPosts(
    filter?: ModelPostFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListPostsQuery> {
    const statement = `query ListPosts($filter: ModelPostFilterInput, $limit: Int, $nextToken: String) {
        listPosts(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            title
            description
            likes {
              __typename
              nextToken
            }
            comments {
              __typename
              nextToken
            }
            createdAt
            updatedAt
            owner
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListPostsQuery>response.data.listPosts;
  }
  async GetLike(id: string): Promise<GetLikeQuery> {
    const statement = `query GetLike($id: ID!) {
        getLike(id: $id) {
          __typename
          id
          post {
            __typename
            id
            title
            description
            likes {
              __typename
              nextToken
            }
            comments {
              __typename
              nextToken
            }
            createdAt
            updatedAt
            owner
          }
          createdAt
          updatedAt
          postLikesId
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetLikeQuery>response.data.getLike;
  }
  async ListLikes(
    filter?: ModelLikeFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListLikesQuery> {
    const statement = `query ListLikes($filter: ModelLikeFilterInput, $limit: Int, $nextToken: String) {
        listLikes(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            post {
              __typename
              id
              title
              description
              createdAt
              updatedAt
              owner
            }
            createdAt
            updatedAt
            postLikesId
            owner
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListLikesQuery>response.data.listLikes;
  }
  async GetComment(id: string): Promise<GetCommentQuery> {
    const statement = `query GetComment($id: ID!) {
        getComment(id: $id) {
          __typename
          id
          post {
            __typename
            id
            title
            description
            likes {
              __typename
              nextToken
            }
            comments {
              __typename
              nextToken
            }
            createdAt
            updatedAt
            owner
          }
          content
          createdAt
          updatedAt
          postCommentsId
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetCommentQuery>response.data.getComment;
  }
  async ListComments(
    filter?: ModelCommentFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListCommentsQuery> {
    const statement = `query ListComments($filter: ModelCommentFilterInput, $limit: Int, $nextToken: String) {
        listComments(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            post {
              __typename
              id
              title
              description
              createdAt
              updatedAt
              owner
            }
            content
            createdAt
            updatedAt
            postCommentsId
            owner
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListCommentsQuery>response.data.listComments;
  }
  OnCreatePostListener(
    owner?: string
  ): Observable<
    SubscriptionResponse<Pick<__SubscriptionContainer, "onCreatePost">>
  > {
    const statement = `subscription OnCreatePost($owner: String) {
        onCreatePost(owner: $owner) {
          __typename
          id
          title
          description
          likes {
            __typename
            items {
              __typename
              id
              createdAt
              updatedAt
              postLikesId
              owner
            }
            nextToken
          }
          comments {
            __typename
            items {
              __typename
              id
              content
              createdAt
              updatedAt
              postCommentsId
              owner
            }
            nextToken
          }
          createdAt
          updatedAt
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (owner) {
      gqlAPIServiceArguments.owner = owner;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<
      SubscriptionResponse<Pick<__SubscriptionContainer, "onCreatePost">>
    >;
  }

  OnUpdatePostListener(
    owner?: string
  ): Observable<
    SubscriptionResponse<Pick<__SubscriptionContainer, "onUpdatePost">>
  > {
    const statement = `subscription OnUpdatePost($owner: String) {
        onUpdatePost(owner: $owner) {
          __typename
          id
          title
          description
          likes {
            __typename
            items {
              __typename
              id
              createdAt
              updatedAt
              postLikesId
              owner
            }
            nextToken
          }
          comments {
            __typename
            items {
              __typename
              id
              content
              createdAt
              updatedAt
              postCommentsId
              owner
            }
            nextToken
          }
          createdAt
          updatedAt
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (owner) {
      gqlAPIServiceArguments.owner = owner;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<
      SubscriptionResponse<Pick<__SubscriptionContainer, "onUpdatePost">>
    >;
  }

  OnDeletePostListener(
    owner?: string
  ): Observable<
    SubscriptionResponse<Pick<__SubscriptionContainer, "onDeletePost">>
  > {
    const statement = `subscription OnDeletePost($owner: String) {
        onDeletePost(owner: $owner) {
          __typename
          id
          title
          description
          likes {
            __typename
            items {
              __typename
              id
              createdAt
              updatedAt
              postLikesId
              owner
            }
            nextToken
          }
          comments {
            __typename
            items {
              __typename
              id
              content
              createdAt
              updatedAt
              postCommentsId
              owner
            }
            nextToken
          }
          createdAt
          updatedAt
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (owner) {
      gqlAPIServiceArguments.owner = owner;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<
      SubscriptionResponse<Pick<__SubscriptionContainer, "onDeletePost">>
    >;
  }

  OnCreateLikeListener(
    owner?: string
  ): Observable<
    SubscriptionResponse<Pick<__SubscriptionContainer, "onCreateLike">>
  > {
    const statement = `subscription OnCreateLike($owner: String) {
        onCreateLike(owner: $owner) {
          __typename
          id
          post {
            __typename
            id
            title
            description
            likes {
              __typename
              nextToken
            }
            comments {
              __typename
              nextToken
            }
            createdAt
            updatedAt
            owner
          }
          createdAt
          updatedAt
          postLikesId
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (owner) {
      gqlAPIServiceArguments.owner = owner;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<
      SubscriptionResponse<Pick<__SubscriptionContainer, "onCreateLike">>
    >;
  }

  OnUpdateLikeListener(
    owner?: string
  ): Observable<
    SubscriptionResponse<Pick<__SubscriptionContainer, "onUpdateLike">>
  > {
    const statement = `subscription OnUpdateLike($owner: String) {
        onUpdateLike(owner: $owner) {
          __typename
          id
          post {
            __typename
            id
            title
            description
            likes {
              __typename
              nextToken
            }
            comments {
              __typename
              nextToken
            }
            createdAt
            updatedAt
            owner
          }
          createdAt
          updatedAt
          postLikesId
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (owner) {
      gqlAPIServiceArguments.owner = owner;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<
      SubscriptionResponse<Pick<__SubscriptionContainer, "onUpdateLike">>
    >;
  }

  OnDeleteLikeListener(
    owner?: string
  ): Observable<
    SubscriptionResponse<Pick<__SubscriptionContainer, "onDeleteLike">>
  > {
    const statement = `subscription OnDeleteLike($owner: String) {
        onDeleteLike(owner: $owner) {
          __typename
          id
          post {
            __typename
            id
            title
            description
            likes {
              __typename
              nextToken
            }
            comments {
              __typename
              nextToken
            }
            createdAt
            updatedAt
            owner
          }
          createdAt
          updatedAt
          postLikesId
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (owner) {
      gqlAPIServiceArguments.owner = owner;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<
      SubscriptionResponse<Pick<__SubscriptionContainer, "onDeleteLike">>
    >;
  }

  OnCreateCommentListener(
    owner?: string
  ): Observable<
    SubscriptionResponse<Pick<__SubscriptionContainer, "onCreateComment">>
  > {
    const statement = `subscription OnCreateComment($owner: String) {
        onCreateComment(owner: $owner) {
          __typename
          id
          post {
            __typename
            id
            title
            description
            likes {
              __typename
              nextToken
            }
            comments {
              __typename
              nextToken
            }
            createdAt
            updatedAt
            owner
          }
          content
          createdAt
          updatedAt
          postCommentsId
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (owner) {
      gqlAPIServiceArguments.owner = owner;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<
      SubscriptionResponse<Pick<__SubscriptionContainer, "onCreateComment">>
    >;
  }

  OnUpdateCommentListener(
    owner?: string
  ): Observable<
    SubscriptionResponse<Pick<__SubscriptionContainer, "onUpdateComment">>
  > {
    const statement = `subscription OnUpdateComment($owner: String) {
        onUpdateComment(owner: $owner) {
          __typename
          id
          post {
            __typename
            id
            title
            description
            likes {
              __typename
              nextToken
            }
            comments {
              __typename
              nextToken
            }
            createdAt
            updatedAt
            owner
          }
          content
          createdAt
          updatedAt
          postCommentsId
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (owner) {
      gqlAPIServiceArguments.owner = owner;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<
      SubscriptionResponse<Pick<__SubscriptionContainer, "onUpdateComment">>
    >;
  }

  OnDeleteCommentListener(
    owner?: string
  ): Observable<
    SubscriptionResponse<Pick<__SubscriptionContainer, "onDeleteComment">>
  > {
    const statement = `subscription OnDeleteComment($owner: String) {
        onDeleteComment(owner: $owner) {
          __typename
          id
          post {
            __typename
            id
            title
            description
            likes {
              __typename
              nextToken
            }
            comments {
              __typename
              nextToken
            }
            createdAt
            updatedAt
            owner
          }
          content
          createdAt
          updatedAt
          postCommentsId
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (owner) {
      gqlAPIServiceArguments.owner = owner;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<
      SubscriptionResponse<Pick<__SubscriptionContainer, "onDeleteComment">>
    >;
  }
}
