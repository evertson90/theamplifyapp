import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ApiComponent } from './components/categories/api/api.component';
import { StorageComponent } from './components/categories/storage/storage.component';
import { PredictionsComponent } from './components/categories/predictions/predictions.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'api', component: ApiComponent },
  { path: 'storage', component: StorageComponent },
  { path: 'predictions', component: PredictionsComponent },
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
