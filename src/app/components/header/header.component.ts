import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Auth } from 'aws-amplify';
import { UserService, UserInfo } from '../../services/shared/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  links = [
    { title: 'Home', path: '/home' },
    { title: 'API', path: '/api' },
    { title: 'Storage', path: '/storage' },
    { title: 'Predictions', path: '/predictions' }
  ];

  activeRoute: string;
  currentUserName: string = '';

  constructor(public router: Router, private userService: UserService) {
    this.activeRoute = router.url;

    router.events.subscribe((val: any) => {
      this.activeRoute = router.url;
    });
    console.log('router url is ' + this.activeRoute);
  }

  ngOnInit(): void {
    this.userService
      .getCurrentUserInfo()
      .then((userInfo: UserInfo) => {
        this.currentUserName = userInfo.username;
      })
      .catch(error => {
        console.log('Error while obtaining user: ', error);
      });
  }

  signOut() {
    Auth.signOut();
  }
}
