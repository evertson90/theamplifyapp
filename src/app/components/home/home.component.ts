import { Component, OnInit } from '@angular/core';
import { CardData } from './homecard/homecard.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  cardDataList: Array<CardData> = [
    {
      imgUrl: 'assets/images/card_1.png',
      title: 'See Amplify in action!',
      description:
        'Test all of the Amplify categories live on this app. Check out the different tabs to get started.'
    },
    {
      imgUrl: 'assets/images/card_2.png',
      title: 'Follow along with the blogs',
      description:
        'Follow along with "The Amplify Series" blogs to be able to create this website yourself using AWS Amplify.'
    },
    {
      imgUrl: 'assets/images/card_3.png',
      title: 'Hosted securely via Amplify',
      description:
        'Using Amplify Hosting, this site is hosted on AWS and the domain was easily configured in the Amplify console'
    }
  ];

  constructor() {}

  ngOnInit(): void {}
}
