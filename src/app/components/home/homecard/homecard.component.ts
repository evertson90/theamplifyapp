import { Component, OnInit, Input } from '@angular/core';

export interface CardData {
  imgUrl: string;
  title: string;
  description: string;
}

@Component({
  selector: 'app-homecard',
  templateUrl: './homecard.component.html',
  styleUrls: ['./homecard.component.css']
})
export class HomecardComponent implements OnInit {
  @Input() cardData: CardData = {
    imgUrl: '',
    title: '',
    description: ''
  };

  constructor() {}

  ngOnInit(): void {}
}
