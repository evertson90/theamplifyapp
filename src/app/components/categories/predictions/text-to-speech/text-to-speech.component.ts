import { Component, OnInit } from '@angular/core';
import { Predictions, Analytics } from 'aws-amplify';
import { TextToSpeechOutput } from '@aws-amplify/predictions';

@Component({
  selector: 'app-text-to-speech',
  templateUrl: './text-to-speech.component.html',
  styleUrls: ['./text-to-speech.component.css']
})
export class TextToSpeechComponent implements OnInit {
  textInput: string | undefined = undefined;

  constructor() {}

  ngOnInit(): void {}

  convertToAudio = async () => {
    if (!this.textInput) {
      return;
    }

    Analytics.record({
      name: 'convertedTextToSpeech',
      attributes: { textInput: this.textInput },
      immediate: true
    });

    Predictions.convert({
      textToSpeech: {
        source: {
          text: this.textInput
        },
        voiceId: 'Amy'
      }
    })
      .then(async result => {
        this.playAudio(result);
      })
      .catch(err => console.log({ err }));
  };

  playAudio = async (audio: TextToSpeechOutput) => {
    const context = new AudioContext();
    const buffer = await context.decodeAudioData(audio.audioStream);
    const source = context.createBufferSource();
    source.buffer = buffer;
    source.connect(context.destination);
    source.start();
  };

  textInputUpdated = (e: Event) => {
    const input = e.target as HTMLInputElement;
    this.textInput = input.value;
  };
}
