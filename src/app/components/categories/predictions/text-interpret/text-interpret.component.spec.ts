import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextInterpretComponent } from './text-interpret.component';

describe('TextInterpretComponent', () => {
  let component: TextInterpretComponent;
  let fixture: ComponentFixture<TextInterpretComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextInterpretComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextInterpretComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
