import { Component, OnInit } from '@angular/core';
import { Predictions } from 'aws-amplify';
import { InterpretTextCategories, InterpretTextOutput } from '@aws-amplify/predictions';

@Component({
  selector: 'app-text-interpret',
  templateUrl: './text-interpret.component.html',
  styleUrls: ['./text-interpret.component.css']
})
export class TextInterpretComponent implements OnInit {
  textInput: string | undefined = undefined;
  interpretation: InterpretTextOutput | undefined = undefined;
  constructor() { }

  ngOnInit(): void {
  }

  interpretText = async () => {
    if (!this.textInput) {
      return;
    }

    Predictions.interpret({
      text: {
        source: {
          text: this.textInput
        },
        type: InterpretTextCategories.ALL
      }
    }).then(result => this.interpretation= result)
    .catch(err => console.log({err}))
  }


  textInputUpdated = (e: Event) => {
    const input = e.target as HTMLInputElement;
    this.textInput = input.value;
  };
}
