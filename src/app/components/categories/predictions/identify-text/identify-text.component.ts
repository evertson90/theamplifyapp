import { Component, OnInit } from '@angular/core';
import { Predictions } from 'aws-amplify';
import { IdentifyTextOutput } from '@aws-amplify/predictions';

@Component({
  selector: 'app-identify-text',
  templateUrl: './identify-text.component.html',
  styleUrls: ['./identify-text.component.css']
})
export class IdentifyTextComponent implements OnInit {
  selectedFile: File | undefined = undefined;
  identifiedText: IdentifyTextOutput | undefined = undefined;

  constructor() {}

  ngOnInit(): void {}

  identifyText = async () => {
    if (!this.selectedFile) {
      return;
    }

    Predictions.identify(
      {
        text: {
          source: {
            file: this.selectedFile
          }
        }
      },
      {}
    )
      .then(response => (this.identifiedText = response))
      .catch(err => console.log({ err }));
  };

  imageSelected = (e: Event) => {
    const input = e.target as HTMLInputElement;

    if (!input.files?.length) {
      return;
    }

    this.selectedFile = input.files[0];
  };
}
