import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentifyTextComponent } from './identify-text.component';

describe('IdentifyTextComponent', () => {
  let component: IdentifyTextComponent;
  let fixture: ComponentFixture<IdentifyTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdentifyTextComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentifyTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
