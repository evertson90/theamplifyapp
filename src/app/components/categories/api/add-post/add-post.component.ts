import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { PostService } from 'src/app/services/api/post.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {
  titleProp = new FormControl('', Validators.required);
  descriptionProp = new FormControl('', Validators.required);
  formHasErrors: boolean = false;

  constructor(private postService: PostService) {}

  ngOnInit(): void {}

  handleOnSubmit() {
    if (!this.titleProp.errors && !this.descriptionProp.errors) {
      this.formHasErrors = false;
      this.postService.addPost(
        this.titleProp.value,
        this.descriptionProp.value
      );

      this.titleProp.setValue('');
      this.descriptionProp.setValue('');
    } else {
      this.formHasErrors = true;
    }
  }
}
