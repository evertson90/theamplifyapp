import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LikeService } from 'src/app/services/api/like.service';
import { Like } from 'src/app/model/like';

@Component({
  selector: 'app-likes',
  templateUrl: './likes.component.html',
  styleUrls: ['./likes.component.css']
})
export class LikesComponent implements OnInit {
  @Input() postId: string = '';

  likes: Like[] = [];

  constructor(
    private modalService: NgbModal,
    private likeService: LikeService
  ) {}

  ngOnInit(): void {
    this.likeService
      .getLikesForPostId(this.postId)
      .subscribe((likes: Like[]) => (this.likes = likes));
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  addLike() {
    this.likeService.addLike(this.postId);
  }
}
