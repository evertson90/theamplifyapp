import { Component, OnInit, TemplateRef, NgModuleRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LikesComponent } from '../likes/likes.component';
import { CommentsComponent } from '../comments/comments.component';
import { PostService } from 'src/app/services/api/post.service';
import { Post } from 'src/app/model/post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts: Post[] = [];

  constructor(
    private modalService: NgbModal,
    private postService: PostService
  ) {}

  ngOnInit(): void {
    this.postService
      .getAllPosts()
      .subscribe((posts: Post[]) => (this.posts = posts));
  }

  openLikesModal(id: string) {
    const modalref = this.modalService.open(LikesComponent);
    modalref.componentInstance.postId = id;
  }

  openCommentsModal(id: string) {
    const modalref = this.modalService.open(CommentsComponent);
    modalref.componentInstance.postId = id;
  }
}
