import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.css']
})
export class ApiComponent implements OnInit {
  titleProp = new FormControl('');
  descriptionProp = new FormControl('');

  constructor() {}

  ngOnInit(): void {}
}
