import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommentService } from 'src/app/services/api/comment.service';
import { FormControl, Validators } from '@angular/forms';
import { Comment } from '../../../../model/comment';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  @Input() postId: string = '';

  comments: Comment[] = [];

  commentProp = new FormControl('', Validators.required);
  formHasErrors: boolean = false;

  constructor(
    private modalService: NgbModal,
    private commentService: CommentService
  ) {}

  ngOnInit(): void {
    this.commentService
      .getCommentsForPostId(this.postId)
      .subscribe((comments: Comment[]) => (this.comments = comments));
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  addComment() {
    if (this.commentProp.errors) {
      this.formHasErrors = true;
      return;
    }

    this.formHasErrors = false;

    const newComment: Comment = {
      postId: this.postId,
      user: 'SomeUser', //Get from Amplify auth later
      date: new Date(),
      content: this.commentProp.value
    };

    this.commentService.addComment(newComment);
    this.commentProp.setValue('');
  }
}
