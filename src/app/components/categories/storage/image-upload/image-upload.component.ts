import { Component, OnInit } from '@angular/core';
import { Storage } from 'aws-amplify';

const MAX_NUMBER_OF_IMAGES_PER_USER: number = 10;

enum UploadState {
  IDLE,
  UPLOADING,
  UPLOAD_COMPLETE,
  ERROR,
  MAX_REACHED
}

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {
  selectedFile: File | undefined = undefined;
  uploadStates = UploadState;
  uploadState: UploadState = UploadState.IDLE;
  progressText: string = '';

  constructor() {}

  ngOnInit(): void {}

  uploadImage = async () => {
    this.uploadState = UploadState.UPLOADING;
    if (!this.selectedFile) {
      return;
    }
    try {
      const userImages = await Storage.list('', { level: 'private' });
      if (userImages.length >= MAX_NUMBER_OF_IMAGES_PER_USER) {
        this.uploadState = UploadState.MAX_REACHED;
        return;
      }

      await Storage.put(this.selectedFile.name, this.selectedFile, {
        contentType: 'image/*',
        level: 'private',
        progressCallback: this.progressCallback
      });
      this.uploadState = UploadState.UPLOAD_COMPLETE;
    } catch (error) {
      this.uploadState = UploadState.ERROR;
      console.log('Error uploading file: ', error);
    }
  };

  progressCallback = (progress: any) => {
    this.progressText = `Uploaded: ${(progress.loaded / progress.total) *
      100} %`;
  };

  imageSelected = (e: Event) => {
    this.uploadState = UploadState.IDLE;
    const input = e.target as HTMLInputElement;

    if (!input.files?.length) {
      return;
    }

    this.selectedFile = input.files[0];
  };
}
