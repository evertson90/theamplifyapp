import { Component, OnInit } from '@angular/core';
import { Storage } from 'aws-amplify';

export interface Image {
  key: string;
  url: string;
}

@Component({
  selector: 'app-image-album',
  templateUrl: './image-album.component.html',
  styleUrls: ['./image-album.component.css']
})
export class ImageAlbumComponent implements OnInit {
  images: Image[] = [];

  constructor() {}

  ngOnInit(): void {
    this.getAllImages();
  }

  getAllImages = () => {
    Storage.list('', { level: 'private' })
      .then(result => {
        result.forEach(async imageObject => {
          const objectKey = imageObject.key;

          if (objectKey !== undefined) {
            const signedURL = await Storage.get(objectKey, {
              level: 'private',
              download: false
            });

            this.images.push({ key: objectKey, url: signedURL });
          }
        });
      })
      .catch(err => console.log(err));
  };

  removeImage = async (key: string) => {
    await Storage.remove(key, { level: 'private' });
    this.images = [];
    this.getAllImages();
  };
}
