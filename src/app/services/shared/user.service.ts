import { Injectable } from '@angular/core';
import { Auth } from 'aws-amplify';

export interface UserInfo {
  email: string;
  username: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor() {}

  async getCurrentUserInfo(): Promise<UserInfo> {
    const userInfoResponse = await Auth.currentUserInfo();
    return {
      email: userInfoResponse.attributes.email,
      username: userInfoResponse.username
    };
  }
}
