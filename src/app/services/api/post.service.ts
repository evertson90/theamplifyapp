import { Injectable } from '@angular/core';
import { Post } from 'src/app/model/post';
import { Observable, BehaviorSubject } from 'rxjs';
import { APIService, Post as AmplifyPost } from 'src/app/API.service';
import { UserService, UserInfo } from '../shared/user.service';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private posts: BehaviorSubject<Post[]> = new BehaviorSubject<Post[]>([]);
  private postsData: Post[] = [];

  constructor(private api: APIService, private userService: UserService) {
    this.setOnPostCreateSubscription();
  }

  private async setOnPostCreateSubscription() {
    const userInfo: UserInfo = await this.userService.getCurrentUserInfo();

    if (userInfo) {
      this.api.OnCreatePostListener(userInfo.username).subscribe(response => {
        const responseData = response.value.data;
        if (responseData && responseData.onCreatePost) {
          const post = this.convertToPost(
            responseData.onCreatePost as AmplifyPost
          );
          this.postsData.push(post);
          this.posts.next(this.postsData);
        }
      });
    }
  }

  addPost(title: string, description: string) {
    this.api.CreatePost({
      title,
      description
    });
  }

  getAllPosts(): Observable<Post[]> {
    this.refreshPosts();
    return this.posts.asObservable();
  }

  private refreshPosts() {
    this.api.CustomListPosts().then(response => {
      const responsePosts: Post[] = [];
      response.items.forEach(item => {
        const post = this.convertToPost(item as AmplifyPost);
        responsePosts.push(post);
      });

      this.postsData = responsePosts;
      this.posts.next(responsePosts);
    });
  }

  private convertToPost(amplifyPost: AmplifyPost): Post {
    const {
      id,
      title,
      description,
      owner,
      createdAt,
      likes,
      comments
    } = amplifyPost;

    const likesItems = likes ? likes.items : undefined;
    const commentsItems = comments ? comments.items : undefined;

    return {
      id,
      title,
      description: description ? description : '',
      author: owner ? owner : '',
      date: new Date(createdAt),
      likes: likesItems ? likesItems.length : 0,
      comments: commentsItems ? commentsItems.length : 0
    };
  }
}
