import { Injectable } from '@angular/core';
import { Comment } from 'src/app/model/comment';
import { BehaviorSubject } from 'rxjs';
import { APIService, Comment as AmplifyComment } from 'src/app/API.service';
import { UserService, UserInfo } from '../shared/user.service';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private comments: BehaviorSubject<Comment[]> = new BehaviorSubject<Comment[]>(
    []
  );
  private commentsData: Comment[] = [];

  constructor(private api: APIService, private userService: UserService) {
    this.setOnCommentCreateSubscription();
  }

  private async setOnCommentCreateSubscription() {
    const userInfo: UserInfo = await this.userService.getCurrentUserInfo();

    if (userInfo) {
      this.api
        .OnCreateCommentListener(userInfo.username)
        .subscribe(response => {
          const responseData = response.value.data;
          if (responseData && responseData.onCreateComment) {
            const comment = this.convertToComment(
              responseData.onCreateComment as AmplifyComment
            );
            this.commentsData.push(comment);
            this.comments.next(this.commentsData);
          }
        });
    }
  }

  addComment({ postId, content }: Comment) {
    this.api.CreateComment({ postCommentsId: postId, content });
  }

  getCommentsForPostId(postId: string) {
    this.api.ListComments({ postCommentsId: { eq: postId } }).then(response => {
      const responseComments: Comment[] = [];
      response.items.forEach(item => {
        const comment = this.convertToComment(item as AmplifyComment);
        responseComments.push(comment);
      });

      this.commentsData = responseComments;
      this.comments.next(responseComments);
    });

    return this.comments;
  }

  private convertToComment(amplifyComment: AmplifyComment): Comment {
    const { postCommentsId, owner, createdAt, content } = amplifyComment;

    return {
      postId: postCommentsId ? postCommentsId : '',
      content,
      user: owner ? owner : '',
      date: new Date(createdAt)
    };
  }
}
