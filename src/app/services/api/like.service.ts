import { Injectable } from '@angular/core';
import { Like } from 'src/app/model/like';
import { BehaviorSubject } from 'rxjs';
import { APIService, Like as AmplifyLike } from 'src/app/API.service';
import { UserService, UserInfo } from '../shared/user.service';

@Injectable({
  providedIn: 'root'
})
export class LikeService {
  private likes: BehaviorSubject<Like[]> = new BehaviorSubject<Like[]>([]);
  private likesData: Like[] = [];

  constructor(private api: APIService, private userService: UserService) {
    this.setOnLikeCreateSubscription();
  }

  private async setOnLikeCreateSubscription() {
    const userInfo: UserInfo = await this.userService.getCurrentUserInfo();

    if (userInfo) {
      this.api.OnCreateLikeListener(userInfo.username).subscribe(response => {
        const responseData = response.value.data;
        if (responseData && responseData.onCreateLike) {
          const like = this.convertToLike(
            responseData.onCreateLike as AmplifyLike
          );
          this.likesData.push(like);
          this.likes.next(this.likesData);
        }
      });
    }
  }

  addLike(postId: string) {
    this.api.CreateLike({
      postLikesId: postId
    });
  }

  getLikesForPostId(postId: string) {
    this.api.ListLikes({ postLikesId: { eq: postId } }).then(response => {
      const responseLikes: Like[] = [];
      response.items.forEach(item => {
        const like = this.convertToLike(item as AmplifyLike);
        responseLikes.push(like);
      });

      this.likesData = responseLikes;
      this.likes.next(responseLikes);
    });

    return this.likes;
  }

  private convertToLike(amplifyLike: AmplifyLike): Like {
    const { postLikesId, owner, createdAt } = amplifyLike;

    return {
      postId: postLikesId ? postLikesId : '',
      user: owner ? owner : '',
      date: new Date(createdAt)
    };
  }
}
