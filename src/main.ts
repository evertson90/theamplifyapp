import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Predictions, Analytics } from 'aws-amplify';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import Amplify from 'aws-amplify';
import aws_exports from './aws-exports';
import { AmazonAIPredictionsProvider } from '@aws-amplify/predictions';
Amplify.configure(aws_exports);
Predictions.addPluggable(new AmazonAIPredictionsProvider());

if (environment.production) {
  enableProdMode();
}

Analytics.autoTrack('pageView', {
  enable: true,
  type: 'SPA',
  attributes: {
    url: window.location.origin + window.location.pathname
  }
});

Analytics.autoTrack('event', {
  enable: true,
  events: ['click'],
  selectorPrefix: 'data-amplify-analytics-'
});

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch(err => console.error(err));
