export type AmplifyDependentResourcesAttributes = {
  "analytics": {
    "theamplifyapp": {
      "Id": "string",
      "Region": "string",
      "appName": "string"
    }
  },
  "api": {
    "theamplifyapp": {
      "GraphQLAPIEndpointOutput": "string",
      "GraphQLAPIIdOutput": "string"
    }
  },
  "auth": {
    "theamplifyappfdeaa7e5": {
      "AppClientID": "string",
      "AppClientIDWeb": "string",
      "IdentityPoolId": "string",
      "IdentityPoolName": "string",
      "UserPoolArn": "string",
      "UserPoolId": "string",
      "UserPoolName": "string"
    }
  },
  "predictions": {
    "identifyText2840d0c6": {
      "format": "string",
      "region": "string"
    },
    "interpretText9001208a": {
      "region": "string",
      "type": "string"
    },
    "speechGenerator11c4cfca": {
      "language": "string",
      "region": "string",
      "voice": "string"
    }
  },
  "storage": {
    "amplifyappimages": {
      "BucketName": "string",
      "Region": "string"
    }
  }
}